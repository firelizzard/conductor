// Copyright (c) 2019 The Conductor Authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package conductor

import (
	"fmt"
	"sync"
)

// type Actor interface {
// 	Name() string
// 	Owner() Actor
// 	Owned(func(string, Actor))
// 	Send(interface{})
// 	Done() <-chan struct{}
// 	Go(name string, routine Routine) (Actor, error)
// }

var rootMu sync.RWMutex
var rootOwned map[string]Actor

type root struct{}

func (root) Name() string          { return "" }
func (root) Owner() Actor          { return Root }
func (root) Done() <-chan struct{} { return nil }
func (root) Owned(f func(Actor))   { owned(&rootMu, &rootOwned, f) }

func (root) Go(name string, routine Routine) (Actor, error) {
	var done bool
	return spawn(Root, &rootMu, &rootOwned, &done, name, routine)
}

func (root) Claim(b Actor) error {
	if b.Owner() != Root {
		panic(fmt.Errorf("%q is not owned by /", FullName(b)))
	}

	var done bool
	return claim(Root, b, &rootMu, &rootOwned, &done)
}

func (root) Send(v interface{}) {
	switch v := v.(type) {
	case Stop, *Stop:
		Root.Owned(func(a Actor) { a.Send(v) })

	case Panicked:
		if OnPanicked != nil {
			OnPanicked(v.Source, v.Value)
		}

	case *Panicked:
		if OnPanicked != nil {
			OnPanicked(v.Source, v.Value)
		}

	case Stopped:
		if OnStopped != nil {
			OnStopped(v.Source, v.Reason)
		}

	case *Stopped:
		if OnStopped != nil {
			OnStopped(v.Source, v.Reason)
		}
	}
}
