// Copyright (c) 2019 The Conductor Authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package conductor

import (
	"errors"
	"fmt"
	"sync"
)

// ErrAlreadyOwns indicates the actor already owns an actor with that name.
var ErrAlreadyOwns = errors.New("already owns")

// ErrDone indicates the actor is done and is not accepting new actors.
var ErrDone = errors.New("done")

type actor struct {
	name     string
	owner    Actor
	mu       sync.RWMutex
	owned    map[string]Actor
	messages chan<- interface{}
	done     chan struct{}
	done2    bool
	sending  sync.WaitGroup
}

func (a *actor) Name() string          { return a.name }
func (a *actor) Owner() Actor          { return a.owner }
func (a *actor) Done() <-chan struct{} { return a.done }
func (a *actor) Owned(f func(Actor))   { owned(&a.mu, &a.owned, f) }

func (a *actor) Send(v interface{}) {
	// don't send the message if the actor is done
	select {
	case <-a.done:
		return
	default:
	}

	// coordinate with cleanup to avoid write-to-closed-channel panic
	a.sending.Add(1)
	defer a.sending.Done()
	a.messages <- v
}

func (a *actor) Go(name string, routine Routine) (Actor, error) {
	return spawn(a, &a.mu, &a.owned, &a.done2, name, routine)
}

func (a *actor) Claim(b Actor) error {
	if b.Owner() != a {
		panic(fmt.Errorf("%q is not owned by %q", FullName(b), FullName(a)))
	}

	return claim(a, b, &a.mu, &a.owned, &a.done2)
}

func owned(mu *sync.RWMutex, owned *map[string]Actor, f func(Actor)) {
	mu.RLock()
	defer mu.RUnlock()

	if *owned == nil {
		return
	}

	for _, actor := range *owned {
		f(actor)
	}
}

func claim(a, b Actor, mu *sync.RWMutex, owned *map[string]Actor, done *bool) error {
	mu.RLock()
	if *done {
		mu.RUnlock()
		return fmt.Errorf("%q is %w", FullName(a), ErrDone)
	}
	mu.RUnlock()

	mu.Lock()
	defer mu.Unlock()

	if *done {
		return fmt.Errorf("%q is %w", FullName(a), ErrDone)
	}

	if _, ok := (*owned)[b.Name()]; ok {
		return fmt.Errorf("%q %w %q", FullName(a), ErrAlreadyOwns, b.Name())
	}

	(*owned)[b.Name()] = b
	return nil
}

func spawn(a Actor, mu *sync.RWMutex, owned *map[string]Actor, done *bool, name string, routine Routine) (*actor, error) {
	b := new(actor)
	b.owner = a
	if name == "" {
		b.name = randStr(10)
	} else {
		b.name = name
	}

	err := claim(a, b, mu, owned, done)
	if err != nil {
		return nil, err
	}

	b.start(routine)
	return b, nil
}

func (a *actor) start(routine Routine) {
	messages := make(chan interface{})
	done := make(chan struct{})
	a.messages, a.done = messages, done

	run := func() (result interface{}) {
		defer func() {
			panicValue := recover()
			if panicValue == nil {
				return
			}

			result = &Panicked{Source: a, Value: panicValue}
		}()

		stopValue := routine(a, messages)
		return &Stopped{Source: a, Reason: stopValue}
	}

	go func() {
		// run the routine
		result := run()

		w := new(sync.WaitGroup)
		w.Add(2)

		// set the primary completion indicator
		close(done)

		// set secondary completion indicator
		go func() {
			a.mu.Lock()
			a.done2 = true
			a.mu.Unlock()
			w.Done()
		}()

		// close messages once all sends are complete
		go func() {
			a.sending.Wait()
			close(messages)
		}()

		// drain messages
		go func() {
			for range messages {
			}
			w.Done()
		}()

		// once cleanup is complete, send result
		go func() {
			w.Wait()
			a.owner.Send(result)
			a.Owned(func(b Actor) { b.Send(result) })
		}()
	}()
}
