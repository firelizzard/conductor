// Copyright (c) 2019 The Conductor Authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package conductor

// A Routine is a function that can be executed as an actor.
type Routine = func(Actor, <-chan interface{}) interface{}

// An Actor is an execution unit.
type Actor interface {
	// Name returns the name of the actor.
	Name() string

	// Owner returns the owner of the actor.
	Owner() Actor

	// Owned calls the given function for each actor owned by the actor.
	Owned(func(Actor))

	// Send sends a message to the actor.
	Send(interface{})

	// Done returns a channel that will block until the actor is done executing.
	Done() <-chan struct{}

	// Go creates and launches an actor owned by the actor.
	Go(name string, routine Routine) (Actor, error)

	// Claim takes ownership of an actor.
	Claim(Actor) error
}

// Root is the root actor from which all other actors are initially launched.
var Root root

// OnPanicked is called when Root receives a Panicked message.
//
// OnPanicked should be modified only before the first call to Root.Go.
var OnPanicked func(Actor, interface{})

// OnStopped is called when Root receives a Stopped message.
//
// OnStopped should be modified only before the first call to Root.Go.
var OnStopped func(Actor, interface{})

// Go creates and launches an actor owned by the root actor.
func Go(name string, routine Routine) (Actor, error) { return Root.Go(name, routine) }
