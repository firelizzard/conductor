// Copyright (c) 2019 The Conductor Authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package conductor

// Panicked indicates that an actor's routine panicked.
type Panicked struct {
	Source Actor
	Value  interface{}
}

// Stop indicates that the receiving actor should stop.
type Stop struct {
	Reason interface{}
}

// Stopped indicates that an actor stopped.
type Stopped struct {
	Source Actor
	Reason interface{}
}
